<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use File;
use Image;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function homeAdmin()
    {
       
        return view('admin.home');
    }


}
