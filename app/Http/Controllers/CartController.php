<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use \Cart as Cart;
use App\Customer;
use App\Order;
use App\Order_detail;
use App\User;
use GuzzleHttp\Client;
use DB;


use Validator;

class CartController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cart');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $duplicates = Cart::search(function ($cartItem, $rowId) use ($request) {
            return $cartItem->id === $request->id;
        });

        if (!$duplicates->isEmpty()) {
            return redirect('cart')->withSuccessMessage('Item is already in your cart!');
        }
        
        Cart::add($request->id, $request->name, 1, $request->price)->associate('App\Product');
        return redirect('cart')->withSuccessMessage('Item was added to your cart!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        // Validation on max quantity
        $validator = Validator::make($request->all(), [
            'quantity' => 'required|numeric|between:1,5'
        ]);

         if ($validator->fails()) {
            session()->flash('error_message', 'Quantity must be between 1 and 5.');
            return response()->json(['success' => false]);
         }

        Cart::update($id, $request->quantity);
        session()->flash('success_message', 'Quantity was updated successfully!');

        return response()->json(['success' => true]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::remove($id);
        return redirect('cart')->withSuccessMessage('Item has been removed!');
    }

    /**
     * Remove the resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function emptyCart()
    {
        Cart::destroy();
        return redirect('cart')->withSuccessMessage('Your cart has been cleared!');
    }

    /**
     * Switch item from shopping cart to wishlist.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function confirmcheckout(){
        
            return view('checkout');
     }
     public function generateInvoice()
     {
         //mengambil data dari table orders
         $order = Order::orderBy('created_at', 'DESC');
         
         //jika sudah terdapat records
         if ($order->count() > 0) {
             //mengambil data pertama yang sdh dishort DESC
             $order = $order->first();
             //explode invoice untuk mendapatkan angkanya
             $explode = explode('-', $order->invoice);
             //angka dari hasil explode di +1
            $int = (int)$explode[1]+1;
            $kode = "INV-";
           $gabung = $kode. $int;
             return $gabung;
         }
         //jika belum terdapat records maka akan me-return INV-1
         return 'INV-1';
     }
    
    //  public function generateOrderid()
    //  {
    //      //mengambil data dari table orders
    //      $order = Order::orderBy('created_at', 'DESC');
    //      //jika sudah terdapat records
    //      if ($order->count() > 0) {
    //          //mengambil data pertama yang sdh dishort DESC
    //          $order = $order->first();
    //         //  //explode invoice untuk mendapatkan angkanya
    //         // $explode = explode('', $order->id);
    //          //angka dari hasil explode di +1
    //             $nmr = $order->id + 1;
    //             return  $nmr;
    //      }
    //      //jika belum terdapat records maka akan me-return INV-1
    //      return '1';
    //  }
    
    
    
    
    
    
     public function storeCheckout(Request $request){
        // for($i=0; $i<count($request->product_id); $i++){
        //     echo json_encode($request->qty[$i]);
        // }  
        // dd($this->generateInvoice());
        $result = Cart::content();
        // dd($result);
        $total = str_replace(',', '', $request->total);  
        
        DB::beginTransaction();
        try{

            $order = Order::create([            
                'invoice' => $this->generateInvoice(),
                'customer_id' => $request->customer_id,
                'user_id' => $request->user_id,
                'total' => $total,
            ]);
            // dd($order);   
            $ambil = Order::orderBy('created_at', 'DESC');            
                //mengambil data pertama yang sdh dishort DESC
                $pertama = $ambil->first();
                $order_id = $pertama->id;
            
            foreach($result as $key => $r){          
                Order_detail::create([  
                // $order->order_detail()->create([                       
                    'order_id' =>  $order_id,
                    'product_id' => $r->id,
                    'qty' =>  $r->qty,                          
                    'price' => $r->subtotal,     
                ]);
            }
            DB::commit();   
            // return redirect()->intended('/konfirmasi');
        }catch(\Exception $e){
            DB::rollback();
            return redirect()->back()->with(['error' => $e->getMessage()]);
        }
              
     }
}
