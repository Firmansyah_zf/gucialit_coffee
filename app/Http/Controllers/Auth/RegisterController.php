<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Admin;
use App\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    // protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->middleware('guest:admin');
        $this->middleware('guest:user');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'phone' => 'required|string|max:20',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    //set method
    public function showAdminRegisterForm()
    {
        return view('auth.register', ['url' => 'admin']);
    }

    public function showUserRegisterForm()
    {
        return view('auth.register', ['url' => 'user']);
    }

    public function generateId()
    {
        //mengambil data dari table orders
        $cust = Customer::orderBy('created_at', 'DESC');
        
        //jika sudah terdapat records
        if ($cust->count() > 0) {
            //mengambil data pertama yang sdh dishort DESC
            $f = $cust->first();
            //explode invoice untuk mendapatkan angkanya
            
            $explode = explode('-', $f->id);
            //angka dari hasil explode di +1
            
           $int = (int)$explode[1]+1;
           
           
           $kode = "CUS-";
           
          $gabung = $kode.$int;
          
            return $gabung;
        }
        //jika belum terdapat records maka akan me-return INV-1
        return 'CUS-1';
    }


    protected function register(Request $request){
        $this->validator($request->all())->validate();
        
        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);

        $ambil = User::orderBy('created_at', 'DESC');            
        //mengambil data pertama yang sdh dishort DESC
        $pertama = $ambil->first();
        $user_id = $pertama->id; 

        $customer = Customer::create([
            'id' => $this->generateId(),
            'user_id' => $user_id,
            'name' => $request['name'],
            'address' => $request['address'],
            'phone' => $request['phone'],
            
        ]);
        
        return redirect()->intended('login/user');
        
    }


    protected function createAdmin(Request $request)
    {
        $this->validator($request->all())->validate();
        $admin = Admin::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
        return redirect()->intended('login/admin');
    }

    protected function createUser(Request $request)
    {
        $this->validator($request->all())->validate();
        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
        
        return redirect()->intended('login/user');
    }
}
