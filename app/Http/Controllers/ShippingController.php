<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
class ShippingController extends Controller
{
    public function getProvince(){
        $client = new Client();
        $client_c = new Client();


        try{
            $response = $client->get('http://api.rajaongkir.com/starter/province',
                array(
                    'headers' => array(
                        'key' => '156103c8deff7708aa6f11b88db991aa',
                    )
                )
            );
            $response_c = $client_c->get('http://api.rajaongkir.com/starter/city',
            array(
                'headers' => array(
                    'key' => '156103c8deff7708aa6f11b88db991aa',
                )
            )
        );

        }catch(RequestException $e){
            var_dump($e->getResponse()->getBody()->getContents());
        }
        $json = $response->getBody()->getContents();
        $json_c = $response_c->getBody()->getContents();

        $prov_result = json_decode($json, true);
        $city_result = json_decode($json_c, true);
        return view('ongkir', compact('prov_result','city_result'));
        // print_r($prov_result);
        // echo "<br>batas--------------------<br><br>";
        // print_r($city_result);
    }

    // public function getCity(){
    //     $client = new Client();
    //     try{
    //         $response = $client->get('http://api.rajaongkir.com/starter/city',
    //             array(
    //                 'headers' => array(
    //                     'key' => '156103c8deff7708aa6f11b88db991aa',
    //                 )
    //             )
    //         );
    //     }catch(RequestException $e){
    //         var_dump($e->getResponse()->getBody()->getContents());
    //     }
    //     $json = $response->getBody()->getContents();

    //     $city_result = json_decode($json, true);

    //     print_r($city_result);
        
    // }

    public function checkShipping(){
        $title = "Check Shipping";
        $city = City::get();
        
    }

    public function prosesShipping(Request $request){
        $client = new Client();
        try{
            $response = $client->request('POST','https://api.rajaongkir.com/starter/cost',
                [
                    'body' => 'origin='.$request->origin.'&destination='.$request->destination.'&weight='.$request->weight.'&courier='.$request->courier.'',
                    'headers' => [
                        'key' => '156103c8deff7708aa6f11b88db991aa',
                        'content-type'=>'application/x-www-form-urlencoded',
                    ]
                ]
                
            );
        }catch(RequestException $e){
            var_dump($e->getResponse()->getBody()->getContents());
        }
        $json = $response->getBody()->getContents();

        $array_result = json_decode($json, true);

        $origin = $array_result["rajaongkir"]["origin_details"]["city_name"];
        $destination = $array_result["rajaongkir"]["origin_details"]["city_name"];
        // print_r($array_result);
        // echo $array_result["rajaongkir"]["results"][0]["costs"][1]["cost"][0]["value"];
        echo $origin; echo  $destination;
        echo "<br><br>batas--------------------<br><br>"; 
        for ($k=0; $k < count($array_result['rajaongkir']['results']); $k++){
            echo"<table> 
                    <tr>
                        <th>No.</th>
                        <th>Layanan.</th>
                        <th>ETD.</th>
                        <th>Tarif.</th>
                    </tr>
            ";
            for ($l=0; $l < count($array_result['rajaongkir']['results'][$k]['costs']); $l++) {
                echo "<tr>
                            <td> {{ $l+1 }} </td>
                            <td> {{$array_result['rajaongkir']['results'][$k]['costs'][$l]['service']}}</td>
                            <td> {{$array_result['rajaongkir']['results'][$k]['costs'][$l]['cost'][0]['etd']}} days</td>
                            <td> {{$array_result['rajaongkir']['results'][$k]['costs'][$l]['cost'][0]['value']}} days</td>
                ";
            }
        }
    }
}
