<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
    protected $fillable = ['id','user_id', 'name', 'address', 'phone'];
    protected $primaryKey = 'id';
    public $incrementing = false; 
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
