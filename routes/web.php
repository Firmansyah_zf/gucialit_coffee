<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/homeadmin', 'HomeController@homeAdmin')->name('homeadmin')->middleware('auth');
Auth::routes();

    
    Route::get('/login/admin', 'Auth\LoginController@showAdminLoginForm');
    Route::get('/login/user', 'Auth\LoginController@showUserLoginForm');
    Route::get('/register/admin', 'Auth\RegisterController@showAdminRegisterForm');
    Route::get('/register/user', 'Auth\RegisterController@showUserRegisterForm');

    Route::post('/login/admin', 'Auth\LoginController@adminLogin');
    Route::post('/login/user', 'Auth\LoginController@userLogin');
    Route::post('/register/admin', 'Auth\RegisterController@createAdmin');
    Route::post('/register/user', 'Auth\RegisterController@createUser');

    //Route::view('/home', 'home')->middleware('auth');
    Route::view('/admin', 'admin.home');
    Route::view('/user', 'home');
   



//admin
Route::resource('/kategori', 'CategoryController')->except([
    'create', 'show'
]);
Route::resource('/produk', 'ProductController');
Route::get('/transaksi', 'OrderController@addOrder');

//user
Route::resource('shop', 'ListController', ['only' => ['index', 'show']]);
Route::resource('cart', 'CartController');
Route::delete('emptyCart', 'CartController@emptyCart');
Route::get('/checkout', 'CartController@confirmcheckout')->name('order.checkout');


Route::post('/buy', 'CartController@storeCheckout')->name('buyorder');
Route::get('/konfirmasi', 'OrderController@confirmOrder');


Route::get('ongkir', 'ShippingController@getProvince');
Route::get('getcity', 'ShippingController@getCity');
Route::get('cekongkir', 'ShippingController@checkShipping');
Route::post('/prosesshipping', 'ShippingController@prosesShipping')->name('proses');
